import {request, gql, GraphQLClient } from 'graphql-request'

const graphqlAPI = process.env.NEXT_PUBLIC_GRAPHCMS_ENDPOIN

export const getPosts = async () => {
    const query = gql`
    query {
      postsConnection {
        edges {
          node {
            author {
              id
              name
              bio
              photo {
                url
              }
            }
            createdAt
            slug
            title
            excerpt
            featuredImage {
              url
            }
            categories {
              name
              slug
            }
          }
        }
      }
    }
    
    `;
  
    const result = await request(graphqlAPI, query);
  
    return result.postsConnection.edges;
};

export const getPostsById = () => {
    return true
}